package com.azamgarath.cc1.entity;

import org.springframework.data.annotation.Id;

import com.azamgarath.cc1.core.EnumProfession;
import com.azamgarath.cc1.core.EnumRace;

public class GameCharacter {

    @Id
    private String id;

    private String name;
    private EnumProfession profession;
    private Integer level;
    private Talents talents;
    private EnumRace race; 

    public GameCharacter() {
    }

    public GameCharacter(String name, EnumProfession profession) {
        this.name = name;
        this.profession = profession;
    }
    
    public GameCharacter(Integer level, Talents talents) {
		this.setLevel(level);
		this.setTalents(talents);
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EnumProfession getProfession() {
        return profession;
    }

    public void setProfession(EnumProfession profession) {
        this.profession = profession;
    }


    public EnumRace getRace() {
		return race;
	}

	public void setRace(EnumRace race) {
		this.race = race;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Talents getTalents() {
		return talents;
	}

	public void setTalents(Talents talents) {
		this.talents = talents;
	}

	@Override
    public String toString() {
        return "GameCharacter{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", race=" + race +
                ", profession=" + profession +
                ", level=" + level +
                ", talents=" + talents + 
                 '}';
    }
}
