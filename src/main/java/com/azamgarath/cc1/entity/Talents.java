package com.azamgarath.cc1.entity;

public class Talents {
	
	private Integer strength;
	private Integer dexterity;
	private Integer intelligence;
	private Integer wisdom;
	private Integer constitution;
	private Integer charisma;
	
	public Talents() {
		strength = 15;
		dexterity = 14;
		intelligence = 13;
		wisdom = 12;
		constitution = 11;
		charisma = 10;
	}
	
	public Integer getStrength () {
		return strength;
	}
	
	public void setStrength(Integer strength) {
		this.strength = strength;
	}

	public Integer getDexterity() {
		return dexterity;
	}

	public void setDexterity(Integer dexterity) {
		this.dexterity = dexterity;
	}

	public Integer getIntelligence() {
		return intelligence;
	}

	public void setIntelligence(Integer intelligence) {
		this.intelligence = intelligence;
	}

	public Integer getWisdom() {
		return wisdom;
	}

	public void setWisdom(Integer wisdom) {
		this.wisdom = wisdom;
	}

	public Integer getConstitution() {
		return constitution;
	}

	public void setConstitution(Integer constitution) {
		this.constitution = constitution;
	}

	public Integer getCharisma() {
		return charisma;
	}

	public void setCharisma(Integer charisma) {
		this.charisma = charisma;
	}
	
	

}
