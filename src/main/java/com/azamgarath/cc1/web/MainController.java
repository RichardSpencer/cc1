package com.azamgarath.cc1.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.azamgarath.cc1.entity.Talents;
import com.azamgarath.cc1.entity.GameCharacter;
import com.azamgarath.cc1.service.GameCharacterService;

@Controller
public class MainController {
    private GameCharacterService gameCharacterService;

    @Autowired
    public void setCharacterService(GameCharacterService gameCharacterService) {
        this.gameCharacterService = gameCharacterService;
    }


    @GetMapping("/characters")
    public String fetchCharacters(Model model){
        model.addAttribute("gamecharacters", gameCharacterService.listCharacters());
        return "gamecharacters";
    }

    @GetMapping("/newcharacter")
    public String displayNew(Model model){
    	Talents talents = new Talents();
    	GameCharacter newCharacter = new GameCharacter (1, talents);
        model.addAttribute("newcharacter", newCharacter);
        return "newcharacter";
    }
    
    @PostMapping("/stage2")
    public String startNew(@ModelAttribute GameCharacter gameCharacter){
    	Talents talents = new Talents();
    	//GameCharacter newCharacter = new GameCharacter (1, talents);
        //model.addAttribute("newcharacter", newCharacter);
        return "gamecharacters";
    }
}