package com.azamgarath.cc1.core;

public enum EnumRace {
	HUMAN,
	ELF,
	HALF_ELF,
	DWARF,
	GNOME,
	HALF_ORC,
	TIEFLING,
	DRAGONBORN

}
