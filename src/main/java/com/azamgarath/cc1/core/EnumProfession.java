package com.azamgarath.cc1.core;

public enum EnumProfession {
	FIGHTER,
	WIZARD,
	CLERIC,
	BARBARIAN,
	BARD,
	WARLOCK,
	ROGUE,
	DRUID
}
