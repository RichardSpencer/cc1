package com.azamgarath.cc1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.azamgarath.cc1.entity.GameCharacter;
import com.azamgarath.cc1.repository.GameCharacterRepository;

@Service
public class GameCharacterServiceImpl implements GameCharacterService {
   
	@Autowired
    private GameCharacterRepository gameCharacterRepository;

    @Override
    public Iterable<GameCharacter> listCharacters() {
        return gameCharacterRepository.findAll();
    }

}
