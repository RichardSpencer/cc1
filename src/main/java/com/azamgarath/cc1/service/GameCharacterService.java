package com.azamgarath.cc1.service;

import com.azamgarath.cc1.entity.GameCharacter;

public interface GameCharacterService {
    Iterable<GameCharacter> listCharacters();
}


