package com.azamgarath.cc1.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import com.azamgarath.cc1.entity.GameCharacter;

public interface GameCharacterRepository extends MongoRepository<GameCharacter, String> {

    public GameCharacter findByName(String name);
    public List<GameCharacter> findByProfession(String profession);

}